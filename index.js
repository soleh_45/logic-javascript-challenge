function mapArray(n, ar){
    if(!ar || ar.length !== n) return 0;

    let arr =0;
    let obj ={};
    for (let i of ar){
        // console.log(obj[i] + "test")
        obj[i] = obj[i] + 1 || 1;
    }

    for (let i of ar){
        obj[i]= obj[i] % 2 === 0
        arr += obj[i]
    }

    return arr;
}



const n =9;
const ar = [10, 20, 20, 10, 10, 30, 50, 10, 20];

console.log(mapArray(n, ar));